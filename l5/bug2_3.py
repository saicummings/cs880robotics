#!/usr/bin/env python

import rospy
from math import *
import numpy as np
import tf
import time
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from sensor_msgs.msg import LaserScan

# for move_base
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *


GOAL_X = 2.5

class Bug2():
    """docstring for Bug2"""
    def __init__(self):
        rospy.init_node('bug2')
        self.odom = Odometry()
        self.starting_odom = Odometry()
        self.bumper_msg = BumperEvent()
        self.twist_msg = Twist()

        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0
        self.straight_distance = 0.0

        self.sleep_time = 0.8
        self.drive_scale = 0.8

        '''
        Things to change while testing on real robot
        '''
        self.cspace = 0.5
        self.drive_scale = 1
        self.goalx = 2.5

        self.bumper_pressed = -1
        self.goal_distance = -1

        self.scan = LaserScan()
        self.scan_sub = rospy.Subscriber('/scan', LaserScan, self.scan_callback, queue_size=10)
        self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback, queue_size=1)
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)
        self.cmd_vel = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=10)

        self.rate = rospy.Rate(50)
        start_time = rospy.Time.now()
        
        print("Waiting, the node is initializing...")
        while(len(self.scan.ranges)  == 0 ):
            time.sleep(self.sleep_time)

        self.bug2_algo()

        rospy.loginfo("Stopping Turtlebot")
        self.cmd_vel.publish(Twist())

    def bug2_algo(self):
        while(self.x < self.goalx):
            self.move_to_goal()
            if ( self.x >= self.goalx):
                return

            self.align_wall()
            self.check_space()

            while( self.check_wall() ):
                self.rotate(-90)
                if ( self.drive_and_check_mline(2) ):
                    print('at m line')
                    self.rotate_right_to_goal()
                    self.move_to_goal()
                    return;
                self.rotate(90)

            self.rotate(45)
            while( True ):
                if( self.check_wall() ):
                    self.rotate(-135)
                    self.drive(1)
                    self.rotate(135)
                else:
                    self.rotate(-45)
                    break

            if ( self.drive_and_check_mline(4) ):
                print('at m line')
                self.rotate_right_to_goal()
                self.move_to_goal()
                return;
            else:
                self.rotate(90)
                self.check_space()

            self.rate.sleep()

    def drive_and_check_mline(self, t):
        time.sleep(self.sleep_time)
        start_sec = rospy.Time.now().to_sec()
        cur_sec = rospy.Time.now().to_sec()
	print(self.y)
        while (cur_sec - start_sec < t and self.y <= 0.1):
            # print(self.y)	
            msg = Twist()
            msg.linear.x = 0.2
            self.cmd_vel.publish(msg)
            cur_sec = rospy.Time.now().to_sec()
            self.rate.sleep()
        msg = Twist()
        self.cmd_vel.publish(msg)

        if ( self.y >= 0.1 ):
            return True

        return False

    def drive(self, t):
        time.sleep(self.sleep_time)
        start_sec = rospy.Time.now().to_sec()
        cur_sec = rospy.Time.now().to_sec()
        while (cur_sec - start_sec < t):
            # print(self.y)
            msg = Twist()
            msg.linear.x = 0.2
            self.cmd_vel.publish(msg)
            cur_sec = rospy.Time.now().to_sec()
            self.rate.sleep()
        msg = Twist()
        self.cmd_vel.publish(msg)

    def move_to_goal(self):
        print('move_to_goal')
        time.sleep(self.sleep_time)
        lzr = self.scan.ranges[len(self.scan.ranges)/2]
        msg = Twist()

        if ( (isnan(lzr) or self.x + lzr > self.goalx ) and self.theta > -1 and self.theta < 1):
            print('going to goalx')
            while (self.x < self.goalx):
                msg.linear.x = 0.2
                self.cmd_vel.publish(msg)
                self.rate.sleep()
        else:
            while (lzr >= self.cspace + 0.1):
                lzr = self.scan.ranges[len(self.scan.ranges)/2]
                if (lzr < self.cspace + 0.2):
                    msg.linear.x = 0.1
                else:
                    msg.linear.x = 0.2
                self.cmd_vel.publish(msg)
                self.rate.sleep()
        msg = Twist()
        self.cmd_vel.publish(msg)

    def check_space(self):
        print('check_space')
        time.sleep(self.sleep_time)
        lzr = self.scan.ranges[len(self.scan.ranges)/2]

        if (isnan(lzr)):
            return

        while (lzr <= self.cspace):
            lzr = self.scan.ranges[len(self.scan.ranges)/2]
            msg = Twist()
            msg.linear.x = -0.1
            self.cmd_vel.publish(msg)
            self.rate.sleep()
        msg = Twist()
        self.cmd_vel.publish(msg)

    def check_wall(self):
        print('check_wall')
        time.sleep(self.sleep_time)
        lzr_arr = self.scan.ranges
        for r in lzr_arr:
            if (isnan(r) == False and r < self.cspace * 1.5):
                return True
        return False
    

    def align_wall(self):
        print('align_wall')
        # time.sleep(self.sleep_time)
        left_lzr = self.scan.ranges[len(self.scan.ranges)/2 + 2]
        right_lzr = self.scan.ranges[len(self.scan.ranges)/2 - 2]

        if (isnan(left_lzr) or isnan(right_lzr)):
            return

        twist_msg = Twist()

        while (left_lzr > right_lzr):
            twist_msg.angular.z = -0.23
            left_lzr = self.scan.ranges[len(self.scan.ranges)/2 + 2]
            right_lzr = self.scan.ranges[len(self.scan.ranges)/2 - 2]
            self.cmd_vel.publish(twist_msg)
            self.rate.sleep()

        twist_msg = Twist()
        self.cmd_vel.publish(twist_msg)

        while (left_lzr < right_lzr):
            twist_msg.angular.z = 0.23
            left_lzr = self.scan.ranges[len(self.scan.ranges)/2 + 5]
            right_lzr = self.scan.ranges[len(self.scan.ranges)/2 - 5]
            self.cmd_vel.publish(twist_msg)
            self.rate.sleep()

        twist_msg = Twist()
        self.cmd_vel.publish(twist_msg)

    def radian2degree(self, radian):
        return radian*57.2958

    def degree2radian(self, degree):
        return degree/57.2958

    def face_to_goal(self):
        pass
    
    def set_linear_goal(self, x_goal, y_goal):
        pass

    def go_to_goal(self, x_goal, y_goal, end_time):
        pass

    def distance(self, x_goal, y_goal):
        return sqrt(((self.x - x_goal)**2)+((self.y - y_goal)**2))

    def scan_callback(self, scan_msg):
        # get an idea from this link about how to read the laser scanner
        self.scan = scan_msg
        # https://gist.github.com/atotto/c47bc69a48ed38e86947b5506b8e0e61

    def bumper_callback(self, bumper_msg):
        pass

    def odom_callback(self, odom_msg):
        # try to save x, y, and theta in your instance variables, for your own convience.
        self.odom = odom_msg
        quaternion = (
            self.odom.pose.pose.orientation.x,
            self.odom.pose.pose.orientation.y,
            self.odom.pose.pose.orientation.z,
            self.odom.pose.pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)
        
        self.theta = euler[2]
        self.x = self.odom.pose.pose.position.x
        self.y = self.odom.pose.pose.position.y

    def bug_angle():
        return atan2(goal.pose.pose.position.y - poser.pose.pose.position.y, goal.pose.pose.position.x- poser.pose.pose.position.x)

    def rotate(self, angle):
        print(angle)
        time.sleep(self.sleep_time)
        twist_msg = Twist()
        twist_msg.angular.z = 0.67 if angle > 0 else -0.67 
        angle = self.normalize_angle(angle)

        goal_rotation = angle + self.radian2degree(self.theta)
        goalmax = goal_rotation
        goalmin = goal_rotation

        if (goal_rotation > 180):
            goal_rotation = goal_rotation - 360

        if (goal_rotation < -180):
            goal_rotation = 360 + goal_rotation        

        to_go = abs(angle)
        
        goalmax = goalmax + 2
        goalmin = goalmin - 2
        if (goalmax > 180):
            goalmax = goalmax - 360

        if (goalmax < -180):
            goalmax = 360 + goalmax

        if (goalmin > 180):
            goalmin = goalmin - 360

        if (goalmin < -180):
            goalmin = 360 + goalmin

        while (False == ( self.radian2degree(self.theta) < goalmax and self.radian2degree(self.theta) > goalmin)):
            self.cmd_vel.publish(twist_msg)
            self.rate.sleep()

        twist_msg = Twist()
        self.cmd_vel.publish(twist_msg)

    def normalize_angle(self, angle):
        angle %= 360
        angle = (angle + 360) % 360
        if angle > 180:
            angle -= 360
        return angle

    def rotate_right_to_goal(self):
        print('rotate_right_to_goal')
        twist_msg = Twist()
        twist_msg.angular.z = -0.67 
        while ( self.theta > 0 ):
            self.cmd_vel.publish(twist_msg)
            self.rate.sleep()

        twist_msg = Twist()
        self.cmd_vel.publish(twist_msg)

if __name__ == '__main__':
    try:
        Bug2()
    except rospy.ROSInterruptException:
        pass

    