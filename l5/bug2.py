#!/usr/bin/env python

import rospy
from math import *
import numpy as np
import tf
import time
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from sensor_msgs.msg import LaserScan

# for move_base
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *

class Bug2():
	def __init__(self):

		# ros related variables
		rospy.init_node('bug2')
		self.odom = Odometry()
		self.starting_odom = Odometry()
		self.bumper_msg = BumperEvent()
		self.twist_msg = Twist()

		self.x = 0.0
		self.y = 0.0
		self.theta = 0.0

		self.straight_distance = 0.0
		self.cspace = 0.2
		self.bumper_pressed = -1
		self.goal_distance = -1

		self.goalx = 0.0
		self.goaly = 0.0

		self.scan = LaserScan()
		self.scan_sub = rospy.Subscriber('/scan', LaserScan, self.scan_callback, queue_size=10)
		self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback, queue_size=1)
		self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)
		self.cmd_vel = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=10)

		# loop rate
		self.rate = rospy.Rate(50)
		start_time = rospy.Time.now()
		


		'''
		Robot movement starts here...
		'''

		print("Waiting, the node is initializing...")
		while(len(self.scan.ranges)	== 0 ):
			time.sleep(1)
		print("Receiving messages...")	
		
		print("Stablizing laser...")	

		x1 = self.x
		y1 = self.y
		x2 = 2.5*cos(self.theta) + x1
		y2 = 2.5*sin(self.theta) + y1
		self.m = (y2-y1)/(x2-x1)
		self.b = y1 - self.m*x1

		self.final_goalx = x2
		self.final_goaly = y2

		lzrange = self.scan.ranges[len(self.scan.ranges)/2] - self.cspace * 3
		print("Nearest object is " + str(lzrange) + " away.") 
		'''
		self.drive_straight(lzrange)
		self.tanLineRotateRight()
		self.lazydrive()

		
		while( self.x + self.cspace < self.final_goalx ):
			self.rotate(85)
			if (self.scan.ranges[len(self.scan.ranges) - 10] < self.cspace * 6):
				self.rotate(-85)
				self.lazydrive()
			else:
				while (self.scan.ranges[len(self.scan.ranges) - 10] > self.cspace * 4):
					self.lazydrive()
				self.lazydrive()
				self.rotate(90)
				lzrange = self.scan.ranges[len(self.scan.ranges)/2] - self.cspace * 3
				self.drive_straight(lzrange)
				self.tanLineRotateRight()
				self.lazydrive()


		'''
		while( False == (self.atGoal(self.final_goalx, self.final_goaly))):
			lzrange = self.scan.ranges[len(self.scan.ranges)/2]
			if ( lzrange - self.cspace >= self.d2goal()):
				self.go_to_goal(self.final_goalx, self.final_goaly, 0)
			else:
				print('Temp goal')
				goal_arr = self.calcGoal(lzrange - 0.7)#1 foot short of obstacle
				self.go_to_goal(goal_arr[0], goal_arr[1], 0)
				b = self.turn_right()
			if ( False == b ):
				break
					
	

		rospy.loginfo("Stopping Turtlebot.")
		self.cmd_vel.publish(Twist())
	
	
	def d2goal(self):
		x = self.x
		y = self.y
		gx = self.final_goalx
		gy = self.final_goaly
		d = sqrt( pow(gx-x,2) + pow(gy-y,2))
		return d
	
	def printPose(self):
		print('x: ' + str(self.x))
		print('y: ' + str(self.y))
		print('theta: ' + str(self.theta))
		print('')

	def onMLine(self):
		x = self.x
		y = self.y
		targety = self.m * x + self.b
		targetx = (y - self.b)/self.m
		if ( (x + self.cspace >= targetx and x - self.cspace <= targetx) or (y + self.cspace >= targety or y - self.cspace <= targety)	):
			return True
		else:
			return False

	def calcGoal(self, final_goalx):
		'''
		Calculate goal based on relative x distance, and returns an
		array [x, y]
		''' 
		x = cos(self.theta)*final_goalx + self.x
		y = sin(self.theta)*final_goalx + self.y

		print('goal x: ' + str(x))
		print('goal y: ' + str(y))
		print('')
		return [x, y]

	def atGoal(self, gx, gy):
		x = self.x
		y = self.y
		if ( (x + self.cspace >= gx and x - self.cspace <= gx) and (y + self.cspace >= gy and y - self.cspace <= gy)):
			return True
		else:
			return False

	def radian2degree(self, radian):
		return radian*57.2958


	def degree2radian(self, degree):
		return degree/57.2958


	def face_to_goal(self):
		x = self.x
		y = self.y
		gx = self.goalx
		gy = self.goaly
		dx = gx - x
		dy = gy - y
		t = 0.0 
		if ( dx != 0.0):
			t = atan(abs(dy/dx))
		tg = 0.0
		if ( dx > 0 and dy > 0):
			tg = t
		elif (dx < 0 and dy > 0):
			tg = 3.1415 - t
		elif (dx < 0 and dy < 0):
			tg = 3.1415 + t
		elif (dx > 0 and dy < 0):
			tg = 2*3.1415 - t
		tr = tg - self.theta
		if ( tr > 3.1415 ):
			tr = (3.1415*2 - tr) * -1.0
		elif ( tr < -3.1415 ):
			tr = tr + 2*3.1415
		
		if (tr < 0.1 and tr > -0.1):#already facing goal
			return
		self.rotate(self.radian2degree(tr))
		

	def turn_right(self):
		self.ssx = self.x
		self.ssy = self.y
		d = 0.6#third of a meter
		
		self.tanLineRotateRight()
		goalarr = self.calcGoal(d)
		#print("goalarr " + str(goalarr))
		time.sleep(1)
		self.go_to_goal(goalarr[0],goalarr[1],0)
		time.sleep(1)
		self.rotate(95)
		while ( False == (self.onMLine())):
			print("calculating tanline")
			time.sleep(3)
			self.tanLineRotateRight()
			print("calculating local goal")
			time.sleep(3)
			goalarr = self.calcGoal(d)
			print("moving to local goal")
			time.sleep(3)
			b = self.move_until_MLine(goalarr[0],goalarr[1])
			if ( b ):
				self.goalx = self.final_goalx
				self.goaly = self.final_goaly
				self.face_to_goal()
				return b
			elif ( self.atGoal(self.ssx,self.ssy) ):
				return False
			print("rotating back")
			time.sleep(3)
			self.rotate(95)
			self.rate.sleep()
		return True
	
			


	def tanLineRotateRight(self):
		curscan = self.scan

		ld1 = -1.0
		lt = 0.0
		ri = -1
		rd1 = -1.0
		rt = 0.0
		amin = curscan.angle_min
		amax = curscan.angle_max
		arc = amax - amin

		#print('arc rad: ' + str(arc) + ' arc deg: ' + str(self.radian2degree(arc)))

		radPI = arc/len(curscan.ranges)
		minI = 0
		#print('radPI rad: ' + str(radPI) + ' radPI deg: ' + str(self.radian2degree(radPI)))
		for  i in range(0,len(curscan.ranges)):
			if( curscan.ranges[i] < curscan.ranges[minI] ):
				minI = i

		li = 0
		ri = 0
		if (minI == 0):
			ri = minI
			li = minI + 4
		elif (minI == 1):
			ri = minI -1
			li = minI + 3
		elif( minI == len(curscan.ranges)-1):
			ri = minI - 4
			li = minI
		elif ( minI == len(curscan.ranges)-2):
			ri = minI - 3
			li = minI + 1
		else:
			ri = minI - 2
			li = minI + 2


		ld1 = curscan.ranges[li]
		lt = amin + li*radPI
		rd1 = curscan.ranges[ri]
		rt = amin + ri*radPI

		maxr = curscan.range_max
		minr = curscan.range_min
		#print('amin: ' + str(amin) + ' amax: ' + str(amax) + '\n')
		'''

		for i in range(0,len(curscan.ranges)):
			# Doing a laser sweep. Saving distance of point on right
			r = curscan.ranges[i]
			if(r < maxr and r > minr):
				rd1 = r
				rt = amin + i * radPI
				ri = i
				break
		
		print('rd1: ' + str(rd1))
		print('rt: ' + str(rt))
		print('')
		
		for i in range(ri+1,len(curscan.ranges)):
			# Continuing a laser sweep. Saving distance of point on left
			r = curscan.ranges[i]
			if ( r == maxr ):
				break
			ld1 = r
			lt = amin + i*radPI

		#print('ld1: ' + str(ld1))
		#print('lt: ' + str(lt))
		#print('')
		'''
		xl = cos(lt)*ld1
		yl = sin(lt)*ld1
		xr = cos(rt)*rd1
		yr = sin(rt)*rd1
		dx = xl-xr
		dy = yl-yr
		theta = 0.0
		if( xl == xr ):
			theta = -pi/2
		elif ( xl > xr ):
			print(self.radian2degree(atan((yl-yr)/(xl-xr))))
			theta = -3.14159265358979323864246/2 + atan((yl-yr)/(xl-xr))
		else:
			print(self.radian2degree(atan((yl-yr)/(xl-xr))))
			theta = atan((yl-yr)/(xl-xr))
		
		print('turn right theta: ' + str(theta) + ' ' + str(self.radian2degree(theta)))

		if(isnan(theta)):
			return False
		self.rotate(self.radian2degree(theta) + 5)
		return True
	

	def set_linear_goal(self, x_goal, y_goal):
		return


	def go_to_goal(self, x_goal, y_goal, end_time):
		vel_msg = Twist()
		self.goalx = x_goal
		self.goaly = y_goal
		self.face_to_goal()
		while( False == (self.atGoal(x_goal,y_goal) )):
			if ( self.atGoal(self.final_goalx, self.final_goaly)):
				return True
			vel_msg.linear.x = 0.1
			self.cmd_vel.publish(vel_msg)
			self.rate.sleep()
			b = self.atGoal(x_goal,y_goal)
			if ( b ):
				print("true")
		
	
	def move_until_MLine(self, x_goal, y_goal):
		vel_msg = Twist()
		self.goalx = x_goal
		self.goaly = y_goal
		self.face_to_goal()
		while( False == (self.atGoal(x_goal,y_goal) )):
			print("test")
			if ( self.onMLine() ): 
				return True
			elif ( self.atGoal(self.ssx,self.ssy)):
				return False
			vel_msg.linear.x = 0.2
			self.cmd_vel.publish(vel_msg)
			self.rate.sleep()
		return False

	def distance(self, x_goal, y_goal):
		return sqrt(((self.x - x_goal)**2)+((self.y - y_goal)**2))


	def scan_callback(self, scan_msg):
		self.scan = scan_msg
		# https://gist.github.com/atotto/c47bc69a48ed38e86947b5506b8e0e61


	def bumper_callback(self, bumper_msg):
		pass


	def odom_callback(self, odom_msg):
		'''
		Had to do some fancy quaternion euler transformation to get theta
		'''

		self.odom = odom_msg

		quaternion = (
			self.odom.pose.pose.orientation.x,
			self.odom.pose.pose.orientation.y,
			self.odom.pose.pose.orientation.z,
			self.odom.pose.pose.orientation.w)
		euler = tf.transformations.euler_from_quaternion(quaternion)
		
		self.theta = euler[2]
		self.x = self.odom.pose.pose.position.x
		self.y = self.odom.pose.pose.position.y


	def bug_angle():
		return atan2(goal.pose.pose.position.y - poser.pose.pose.position.y, goal.pose.pose.position.x- poser.pose.pose.position.x)


	def drive_straight(self, distance):
		

		goalx = cos(self.theta)*distance + self.x

		#print ( self.x )
		#print(goalx)
		twist_msg = Twist()

		while ( self.x < goalx):
			lzrange = self.scan.ranges[len(self.scan.ranges)/2]
			if ( lzrange <= self.cspace * 2.5):
				self.rotate(-45)
				return
			twist_msg.linear.x = 0.2
			self.cmd_vel.publish(twist_msg)

		twist_msg = Twist()
		self.cmd_vel.publish(twist_msg)

	def lazydrive(self):
		# Drive straigh for 2 seconds
		lzrange = self.scan.ranges[len(self.scan.ranges)/2]



		st = rospy.Time.now().to_sec()
		curt = rospy.Time.now().to_sec()
		while (curt - st < 2.5):
			msg = Twist()
			msg.linear.x = 0.2
			self.cmd_vel.publish(msg)
			curt = rospy.Time.now().to_sec()
			self.rate.sleep()

		twist_msg = Twist()
		self.cmd_vel.publish(twist_msg)


	def rotate(self, angle):
		twist_msg = Twist()
		twist_msg.angular.z = 1 if angle > 0 else -1 
		angle = self.normalize_angle(angle)
		# print("Rotating %.4f degrees" % angle)


		goal_rotation = angle + self.radian2degree(self.theta)
		goalmax = goal_rotation
		goalmin = goal_rotation

		if (goal_rotation > 180):
			goal_rotation = goal_rotation - 360

		if (goal_rotation < -180):
			goal_rotation = 360 + goal_rotation

		# print('cur theta ' + str(self.radian2degree(self.theta)))
		

		to_go = abs(angle)
		

		# print('goal theta ' + str(goal_rotation))
		# print('to_go theta ' + str(to_go))

		goalmax = goalmax + 2
		goalmin = goalmin - 2
		if (goalmax > 180):
			goalmax = goalmax - 360

		if (goalmax < -180):
			goalmax = 360 + goalmax

		if (goalmin > 180):
			goalmin = goalmin - 360

		if (goalmin < -180):
			goalmin = 360 + goalmin

		while (False == ( self.radian2degree(self.theta) < goalmax and self.radian2degree(self.theta) > goalmin)):
			self.cmd_vel.publish(twist_msg)

		twist_msg = Twist()
		self.cmd_vel.publish(twist_msg)


		'''
			angle = self.degree2radian(angle)
			if( angle > 0):
				curtheta = self.theta
				angle = angle + curtheta
				while(curtheta <= angle):
					# print("curtheta " + str(curtheta))
					self.cmd_vel.publish(twist_msg)
					curtheta = self.theta
					self.rate.sleep()
			else:
				curtheta =self.theta - 2*3.14159265
				# print("Slef.thea " + str(self.radian2degree(self.theta)))
				# print("curtheta " + str(self.radian2degree(curtheta)))
				angle = angle + curtheta
				# print("angle " + str(self.radian2degree(angle)))
				while( curtheta >= angle):
					#print("curtheta " + str(self.radian2degree(curtheta)))
					self.cmd_vel.publish(twist_msg)
					curtheta =self.theta - 2*3.14159265
					self.rate.sleep()
		'''
	
	

	def rotation_distance(self):
		"""Calculate the difference in yaw between two quaternions"""
		(r1, p1, y1) = tf.transformations.euler_from_quaternion([self.odom.pose.pose.orientation.x,
																 self.odom.pose.pose.orientation.y,
																 self.odom.pose.pose.orientation.z,
																 self.odom.pose.pose.orientation.w])
		(r2, p2, y2) = tf.transformations.euler_from_quaternion([self.starting_odom.pose.pose.orientation.x,
																 self.starting_odom.pose.pose.orientation.y,
																 self.starting_odom.pose.pose.orientation.z,
																 self.starting_odom.pose.pose.orientation.w])
		# r, p, y now contain the roll, pitch and yaw from the two quaternions
		# you can derive the angle from that in a number of ways
		euler = abs(y2 - y1)
		yaw_degrees = euler * 180.0 / 3.1415
		# yaw_degrees += 360.0 if (yaw_degrees < 0) else yaw_degrees
		# print yaw_degrees
		return yaw_degrees

	def normalize_angle(self, angle):
		"""REDUCE ANGLES TO -180 180"""
		angle %= 360
		angle = (angle + 360) % 360
		if angle > 180:
			angle -= 360
		return angle

if __name__ == '__main__':
	try:
		Bug2()
	except rospy.ROSInterruptException:
		pass

	
