#!/usr/bin/env python
import rospy
import math
import numpy as np
import copy as cm
import tf
from copy import deepcopy
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState

BUMPERS = ["LEFT", "CENTER", "RIGHT"]
STATUS = ["STOPPED", "STRAIGHT", "ROTATING", "ARC"]

# TURTLEBOT SPECS

MAX_LIN_VEL = .7
MAX_ROT_VEL = 3.14

BASE_DIAMETER = .23
WHEEL_RADIUS = .035
WHEEL_WIDTH = .021
TICKS_PER_REVOLUTION = 52
PULSES_PER_REVOLUTION = 13
TICK_TO_METER = .000085292090497737556558
METER_TO_TICK = 11724.41658029856624751591
MILLIMITER_TO_TICK = 11.72441658029856624751591
TICK_TO_RADIAN = .002436916871363930187454

# THRESHOLDS CHANGE THOSE AS YOU SEEM FIT
# x, y distance form target to consider correct
XY_THRES = .05
# same for rotation angle
ROT_THRES = 5
# same for arc movement
QUAT_THRES = .01


class RobotStatus:
    # helper class with possible robot states
    # not necessary for implementation
    STOPPED, STRAIGHT, ROTATING, ARC = range(4)

    def __init__(self):
        pass


def normalize_angle(angle):
    """REDUCE ANGLES TO -180 180"""
    angle %= 360
    angle = (angle + 360) % 360
    if angle > 180:
        angle -= 360
    return angle


class Lab2Solution:
    def vel_from_wheels(self, phi_right, phi_left):
        """takes in right and left wheel velocities and translates the to twist messages"""
        # Determines the linear velocity of base based on the wheels
        lin_vel = .5 * (phi_right + phi_left)
        # Determines the angular velocity of base bread on the wheels.
        ang_vel = (1 / BASE_DIAMETER) * (phi_right - phi_left)
        self.twist_msg.linear.x = lin_vel
        self.twist_msg.angular.z = ang_vel

    def copy_odom(self, message=-1):
        """
            copy an odometry message (deepcopy did not work for me
            if you figure it out let me know :P).
            If no arguments are given it will the message the solution
            uses
        """
        if message == -1:
            message = self.odom
        ret = Odometry()
        ret.pose.pose.position.x = message.pose.pose.position.x
        ret.pose.pose.position.y = message.pose.pose.position.y
        ret.pose.pose.position.z = message.pose.pose.position.z
        ret.pose.pose.orientation.x = message.pose.pose.orientation.x
        ret.pose.pose.orientation.x = message.pose.pose.orientation.x
        ret.pose.pose.orientation.y = message.pose.pose.orientation.y
        ret.pose.pose.orientation.z = message.pose.pose.orientation.z
        ret.pose.pose.orientation.w = message.pose.pose.orientation.w
        ret.twist.twist.linear.x = message.twist.twist.linear.x
        ret.twist.twist.linear.y = message.twist.twist.linear.y
        ret.twist.twist.linear.z = message.twist.twist.linear.z
        ret.twist.twist.angular.x = message.twist.twist.angular.x
        ret.twist.twist.angular.y = message.twist.twist.angular.y
        ret.twist.twist.angular.z = message.twist.twist.angular.z
        return ret

    def euclidean_distance(self):
        """Calculate euclidean distance between two points"""
        print self.starting_odom.pose.pose.position.x
        print self.starting_odom.pose.pose.position.y
        print self.odom.pose.pose.position.x
        print self.odom.pose.pose.position.y
        dis = np.sqrt(math.pow((self.starting_odom.pose.pose.position.x - self.odom.pose.pose.position.x), 2) +
                      math.pow((self.starting_odom.pose.pose.position.y - self.odom.pose.pose.position.y), 2))
        # print dis
        return dis

    def rotation_distance(self):
        """Calculate the difference in yaw between two quaternions"""
        (r1, p1, y1) = tf.transformations.euler_from_quaternion([self.odom.pose.pose.orientation.x,
                                                                 self.odom.pose.pose.orientation.y,
                                                                 self.odom.pose.pose.orientation.z,
                                                                 self.odom.pose.pose.orientation.w])
        (r2, p2, y2) = tf.transformations.euler_from_quaternion([self.starting_odom.pose.pose.orientation.x,
                                                                 self.starting_odom.pose.pose.orientation.y,
                                                                 self.starting_odom.pose.pose.orientation.z,
                                                                 self.starting_odom.pose.pose.orientation.w])
        # r, p, y now contain the roll, pitch and yaw from the two quaternions
        # you can derive the angle from that in a number of ways
        euler = abs(y2 - y1)
        yaw_degrees = euler * 180.0 / math.pi
        # yaw_degrees += 360.0 if (yaw_degrees < 0) else yaw_degrees
        # print yaw_degrees
        return yaw_degrees

    def odom_callback(self, odom_msg):
        """ callback to handle odometry messages"""
        self.odom = odom_msg

        # self.odom_tf_broadcaster.sendTransform((odom_msg.pose.pose.position.x, odom_msg.pose.pose.position.y, 0),
        #                            (odom_msg.pose.pose.orientation.x, odom_msg.pose.pose.orientation.y,
        #                             odom_msg.pose.pose.orientation.z,
        #                             odom_msg.pose.pose.orientation.w),
        #                            rospy.Time.now(),
        #                            "base_footprint",
        #                            "odom")
        return

    def encoder_callback(self, encoder_msg):
        """
        callback to handle joint states
        could read individual wheel
        velocities and positions
        """
        # print encoder_msg
        self.right_wheel_vel = encoder_msg.velocity[encoder_msg.name.index('wheel_right_joint')]
        self.right_wheel_pos = encoder_msg.position[encoder_msg.name.index('wheel_right_joint')]
        self.left_wheel_vel = encoder_msg.velocity[encoder_msg.name.index('wheel_left_joint')]
        self.left_wheel_pos = encoder_msg.position[encoder_msg.name.index('wheel_left_joint')]

    def clear_list(self):
        """
        You may want to implement a function that cancels given action(s)
        """
        self.command_list = []

    def cancel_goals(self):
        """
        You may want to implement a function that stops movement
        """
        rospy.logerr("Cancelling All Movement")
        self.status = RobotStatus.STOPPED
        self.goal_rotation = -1
        self.goal_distance = -1
        self.twist_msg = Twist()
        # self.command_list = []

    def drive_straight(self, speed, distance, time=.1):
        """ IMPLEMENT YOUR OWN HERE """
        print("Going straight for %.2f meters at %.2f m/s" % (distance, speed))
        # self.starting_odom = cm.deepcopy(self.odom)
        self.starting_odom = self.copy_odom()
        self.goal_distance = distance
        self.status = RobotStatus.STRAIGHT
        self.twist_msg = Twist()
        if speed >= 0:
            self.twist_msg.linear.x = MAX_LIN_VEL if speed > MAX_LIN_VEL else speed
        else:
            # linvel = -MAX_LIN_VEL if speed < -MAX_LIN_VEL else speed
            rospy.logwarn("You are moving backwards, cancelling")
        pass

    def rotate(self, angle):
        """ IMPLEMENT YOUR OWN HERE """
        self.twist_msg = Twist()
        self.twist_msg.angular.z = 1 if angle > 0 else -1
        # self.starting_odom = cm.deepcopy(self.odom)
        self.starting_odom = self.copy_odom()

        angle = normalize_angle(angle)
        print("Rotating %.4f degrees" % angle)

        self.goal_rotation = abs(angle)
        self.status = RobotStatus.ROTATING

    def drive_arc(self, radius, speed=0.1, angle=0):
        """ IMPLEMENT YOUR OWN HERE """
        # self.starting_odom = cm.deepcopy(self.odom)
        self.starting_odom = self.copy_odom()
        w = speed / radius
        v1 = w * (radius + .5 * BASE_DIAMETER)
        v2 = w * (radius - .5 * BASE_DIAMETER)

        angle = normalize_angle(angle)
        print("Rotating %.4f degrees around a %.2f m radius" % (angle, radius))
        self.current_tf = tf.TransformerROS()
        rotation = np.array([[math.cos(angle), -math.sin(angle), 0],
                             [math.sin(angle), math.cos(angle), 0],
                             [0, 0, 1]])

        (trans, rot) = self.odom_listener.lookupTransform('odom', 'base_link', rospy.Time(0))
        print trans
        print rot
        t_o_t = self.current_tf.fromTranslationRotation(trans, rot)
        r_o_t = t_o_t[0:3, 0:3]
        print("TOT")
        print t_o_t
        print("ROT")
        print r_o_t
        print("################")
        self.goal_arc = np.dot(rotation, r_o_t)
        self.status = RobotStatus.ARC
        self.vel_from_wheels(v2, v1) if angle > 0 else self.vel_from_wheels(v1, v2)

    def execute_trajectory(self):
        """ IMPLEMENT YOUR OWN HERE """
        # self.command_list.append([self.drive_straight, .3, .6])
        self.command_list.append([self.rotate, -90])
        # self.command_list.append([self.drive_arc, .15, .2, -180])
        # self.command_list.append([self.rotate, -135])
        # self.command_list.append([self.drive_straight, .3, .42])

    def process_position(self):
        """
        you can check the progress of your actions here
        or in the individual function, it's up to you
        """
        if self.status == RobotStatus.STRAIGHT:
            ed = self.euclidean_distance()
            to_go = abs(ed - self.goal_distance)
            rospy.logwarn("%.4f meters travelled %.4f meters to go" % (ed, to_go))
            if to_go < XY_THRES:
                self.cancel_goals()
        elif self.status == RobotStatus.ROTATING:
            rd = abs(self.rotation_distance())
            to_go = abs(rd - self.goal_rotation)
            rospy.logwarn("%.4f degrees rotated %.4f degrees to go" % (rd, abs(rd - self.goal_rotation)))
            if to_go < ROT_THRES:
                self.cancel_goals()
        elif self.status == RobotStatus.ARC:
            (trans, rot) = self.odom_listener.lookupTransform('odom', 'base_link', rospy.Time(0))
            transformer = tf.TransformerROS()
            state = transformer.fromTranslationRotation(trans, rot)
            state_rot = state[0:3, 0:3]
            x = abs((state_rot - self.goal_arc))
            print x
            x = x < QUAT_THRES
            if x.all():
                self.cancel_goals()
        else:
            self.cancel_goals()

    def shutdown(self):
        """ publish an empty twist message to stop the turtlebot"""
        rospy.loginfo("Stopping Turtlebot")
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)

    def __init__(self):
        """ STUFF YOU MAY WANT TO USE"""
        rospy.init_node('lab1_2')
        rospy.on_shutdown(self.shutdown)
        # odometry messages
        self.odom = Odometry()
        self.starting_odom = Odometry()
        # bumper event message
        self.bumper_msg = BumperEvent()
        # twist message
        self.twist_msg = Twist()

        # transformer
        self.current_tf = tf.TransformerROS()
        self.current_transform = []
        self.current_rotation = []

        # transform listener
        self.odom_listener = tf.TransformListener()
        # transform broadcaster
        self.odom_tf_broadcaster = tf.TransformBroadcaster()

        # placeholder variables
        self.right_wheel_vel = 0.0
        self.right_wheel_pos = 0.0
        self.left_wheel_vel = 0.0
        self.left_wheel_pos = 0.0

        # things you might want to consider using
        self.bumper_pressed = -1
        self.goal_distance = -1
        self.goal_rotation = -1
        self.goal_arc = []
        self.command_list = []
        self.status = RobotStatus.STOPPED  # -1 to stop, 1 to move

        # loop rate
        r = rospy.Rate(50)

        # SUBSCRIBERS AND PUBLISHERS
        # you might need to change topics depending on
        # whether you use simulation or real robots
        # subscribe to bumper events
        # self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback, queue_size=1)
        # subscribe to odometry messages
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)
        # subscribe to encoder messages
        self.encoder_sub = rospy.Subscriber('/joint_states', JointState, self.encoder_callback, queue_size=1)
        # publisher for twist messages
        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)

        start_time = rospy.Time.now()
        trig = True
        while not rospy.is_shutdown():
            '''
            # IN THIS LOOP ROS SENDS AND RECEIVES
            if rospy.Time.now().to_sec() - start_time.to_sec() > 1:
                start_time = rospy.Time.now()
                # LOGGING MESSAGES FOR DEBUGGING
                rospy.logwarn(
                    "Starting Position %.2f %.2f %.2f" % (self.starting_odom.pose.pose.position.x,
                                                          self.starting_odom.pose.pose.position.y,
                                                          self.starting_odom.pose.pose.position.z))
                rospy.logwarn(
                    "Current Position %.2f %.2f %.2f" % (self.odom.pose.pose.position.x,
                                                         self.odom.pose.pose.position.y,
                                                         self.odom.pose.pose.position.z))
                # rospy.logwarn("Linear Velocity %f %f %f" % (self.odom.twist.twist.linear.x,
                #                                             self.odom.twist.twist.linear.y,
                #                                             self.odom.twist.twist.linear.z))
                # rospy.logwarn("Angular Velocity %f %f %f" % (self.odom.twist.twist.angular.x,
                #                                              self.odom.twist.twist.angular.y,
                #                                              self.odom.twist.twist.angular.z))
                rospy.logwarn("Wheels Velocity R:%.2f L:%.2f" % (self.right_wheel_vel, self.left_wheel_vel))
                rospy.logwarn("Wheels Position R:%.2f L:%.2f" % (self.right_wheel_pos, self.left_wheel_pos))
                rospy.logwarn("Bumper Status %d " % self.bumper_pressed)
                rospy.logwarn("Robot Status %d " % self.status)
                rospy.logwarn("%d Commands to be executed" % len(self.command_list))
            # if self.bumper_pressed == 1:
            if trig:
                self.execute_trajectory()
                rospy.sleep(3)
                self.bumper_pressed = -1
                trig = False

            if (self.status == RobotStatus.STOPPED) & (len(self.command_list) > 0):
                # print self.command_list
                f = self.command_list.pop(0)
                f[0](*f[1:])
            if self.status != RobotStatus.STOPPED:
                self.process_position()
            '''
            self.execute_trajectory()
            rospy.sleep(3)
            self.cmd_vel.publish(self.twist_msg)
            r.sleep()

        return


if __name__ == '__main__':
    try:
        Lab2Solution()
    except rospy.ROSInterruptException:
        pass
