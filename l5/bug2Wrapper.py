#!/usr/bin/env python

import rospy
from math import *
import numpy as np
import tf
import time
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from sensor_msgs.msg import LaserScan

# for move_base
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *


GOAL_X = 2.5

class Bug2():
    """docstring for Bug2"""
    def __init__(self):

        # ros related variables
        rospy.init_node('bug2')
        # odometry messages
        self.odom = Odometry()
        self.starting_odom = Odometry()
        # bumper event message
        self.bumper_msg = BumperEvent()
        # twist message
        self.twist_msg = Twist()

        # robot's position and direction
        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0
        self.straight_distance = 0.0
        self.cSpace = 0.15

        # things you might want to consider using
        self.bumper_pressed = -1
        self.goal_distance = -1

        # reading the laser data
        self.scan = LaserScan()
        self.scan_sub = rospy.Subscriber('/scan', LaserScan, self.scan_callback, queue_size=10)

        self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback, queue_size=1)
        # subscribe to odometry messages
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)
        # publisher for twist messages
        self.cmd_vel = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=10)

        # loop rate
        rate = rospy.Rate(50)
        start_time = rospy.Time.now()
        
        # wait until specific amount of time, until your messeges start to be received by your node
        print("Waiting, the node is initializing...")
        while(len(self.scan.ranges)  == 0 ):
            time.sleep(1)
        
        # set the goal
        self.goalx = 0.0
        self.goaly = 0.0
        self.status = 0
        # 0 = goal seek, 1 = wall follow

        # you might want to define a temporary goal for when the bot face towards a wall
        
       # stablize laser
        self.drive_straight(0.1, 0.1)
        print(len(self.scan.ranges))
        self.goalx = self.scan.ranges[len(self.scan.ranges)/2] - self.cSpace
        goal_distance = self.goalx
        
        print(self.goalx)
        print(self.goalx) 
        print(self.goalx) 
        print(self.goalx) 

        # No obstacle
        if ( self.goalx >= GOAL_X ):
            while ( self.odom.pose.pose.position.x < GOAL_X ):
                self.drive_straight(0.3,0.3)
        else:
            while (not rospy.is_shutdown() ):
                x = self.odom.pose.pose.position.x
                y = self.odom.pose.pose.position.y
                theta = self.odom.pose.pose.orientation.z

                if (self.status == 0):
                    # Move to the obstacle
                    d = self.scan.ranges[len(self.scan.ranges)/2] - self.cSpace
                    if ( d >= self.goal_distance):
                        self.drive_straight(0.3,0.3)
                    else:
                        self.turn_right()
                        self.status = 1
                        start_time = rospy.Time.now()
                else: 
                    if (rospy.Time.now() - start_time < 4):
                        drive_straight(0.3,0.3)
                        if ( y < 0.1 and y > -0.1):
                            face_to_goal()
                        self.status == 0
                    else:
                        rotate(90)
                        self.turn_right()
                        start_time = rospy.Time.now()

            # implement bug2 algorithm
            
            # you might want to print your current position and your tmp and actual goal here

        rospy.loginfo("Stopping Turtlebot")
        self.cmd_vel.publish(Twist())
    
    def radian2degree(self, radian):
        return radian*57.2958

    def degree2radian(self, degree):
        return degree/57.2958

# Following are some functions that you may want to implement for # this lab. 

    def face_to_goal(self):
        # turn the robot until it faces towards the goal
        x = self.odom.pose.pose.position.x
        y = self.odom.pose.pose.position.y
        theta = self.odom.pose.pose.orientation.z
        
        turningTheta = atan((self.goaly - y)/(self.goalx - x))
        
        if (turningTheta < 0.1 and turningTheta > -0.1):
            return
        
        rotate(radian2degree(turningTheta))
        
    def turn_right(self):
        self.tanLineRotateRight()
        '''
        drive_straight(0.3,0.3)
        while( self.odom.pose.pose.position.y < 0):
            rotate(90)
            self.tanlineRotateRight()
            drive_straight(0.3,0.3)
            '''
        # you want to turn to your right when you see an obstacle, or maybe left. Up to you, doesn't really matter.
    
    def tanLineRotateRight(self):
        ld1 = -1.0
        lt = 0.0
        li = -1
        rd1 = -1.0
        rt = 0.0
        amin = self.scan.angle_min
        amax = self.scan.angle_max
        arc = amax - amin
        radPI = arc/len(self.scan.ranges)
        maxr = self.scan.range_max
        minr = self.scan.range_min
        for i in range(0,len(self.scan.ranges)):
            r = self.scan.ranges[i]
            if(r<maxr and r>minr):
                ld1 = r
                lt = amin + i*radPI
                li = i
                break
        for i in range(li+1,len(self.scan.ranges)):
            r = self.scan.ranges[i]
            if ( r == maxr ):
                break
            rd1 = r
            rt = amin + i*radPI
        xl = cos(lt)*ld1
        yl = sin(lt)*ld1
        xr = cos(rt)*rd1
        yr = sin(rt)*rd1
        dx = xl-xr
        dy = yl-yr
        theta = 0.0
        if( xl == xr ):
            theta = pi/2
        elif ( xl > xr ):
            theta = 3.14159265358979323864246 - atan((xl-xr)/(yl-yr))
        else:
            theta = atan((xl-xr)/(yl-yr))
        theta = -1 * theta
        self.rotate(self.radian2degree(theta))
    
    def set_linear_goal(self, x_goal, y_goal):
        # If you are using codes from Lab 4: when the robot is far away from a goal, set a relevently large value for your speed. Not that large though. 
        # when the robot gets close to a goal, set a small value for your speed. If you design a P controller, you don't need to do this manually. 
        pass

    def go_to_goal(self, x_goal, y_goal, end_time):
        # make it move towards the goal
        
        
        # don't forget to use sleep function to sync your while loop with the frequency of other nodes

        # maybe it's not a bad idea to publish an empty twist message to reset everything at the end of this function
        self.cmd_vel.publish(Twist())

        if (distance_to_goal < min_distance_to_your_goal):
            # we made it to the goal
            print "Yay, we made it"
            return True
        else:
            # an error happened
            print "go to goal has failed"
            return False

    def distance(self, x_goal, y_goal):
        return sqrt(((self.x - x_goal)**2)+((self.y - y_goal)**2))

    def scan_callback(self, scan_msg):
        # get an idea from this link about how to read the laser scanner
        self.scan = scan_msg
        # https://gist.github.com/atotto/c47bc69a48ed38e86947b5506b8e0e61



    def bumper_callback(self, bumper_msg):
        pass

    def odom_callback(self, odom_msg):
        # try to save x, y, and theta in your instance variables, for your own convience.
        self.odom = odom_msg

    def bug_angle():
        return atan2(goal.pose.pose.position.y - poser.pose.pose.position.y, goal.pose.pose.position.x- poser.pose.pose.position.x)




    def drive_straight(self, speed, distance, time=.1):
        # self.starting_odom = cm.deepcopy(self.odom)
        # self.starting_odom = self.copy_odom()
        # self.goal_distance = distance
        # self.status = RobotStatus.STRAIGHT
    
        if speed >= 0:
            msg = Twist()
            msg.linear.x = speed
            self.cmd_vel.publish(msg)
        
    
    def rotate(self, angle):
        self.twist_msg = Twist()
        self.twist_msg.angular.z = 1 if angle > 0 else -1
        # self.starting_odom = cm.deepcopy(self.odom)
        # self.starting_odom = self.copy_odom()
        
        angle = self.normalize_angle(angle)
        print("Rotating %.4f degrees" % angle)
        self.goal_rotation = abs(angle)
      
        
        self.cmd_vel.publish(self.twist_msg)

    def normalize_angle(self, angle):
        """REDUCE ANGLES TO -180 180"""
        angle %= 360
        angle = (angle + 360) % 360
        if angle > 180:
            angle -= 360
        return angle



if __name__ == '__main__':
    try:
        Bug2()
    except rospy.ROSInterruptException:
        pass

    