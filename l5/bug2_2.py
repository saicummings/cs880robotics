#!/usr/bin/env python

import rospy
from math import *
import numpy as np
import tf
import time
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from sensor_msgs.msg import LaserScan

# for move_base
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *


GOAL_X = 2.5

class Bug2():
    """docstring for Bug2"""
    def __init__(self):
        rospy.init_node('bug2')
        self.odom = Odometry()
        self.starting_odom = Odometry()
        self.bumper_msg = BumperEvent()
        self.twist_msg = Twist()

        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0
        self.straight_distance = 0.0
        self.cspace = 0.15

        self.bumper_pressed = -1
        self.goal_distance = -1

        self.scan = LaserScan()
        self.scan_sub = rospy.Subscriber('/scan', LaserScan, self.scan_callback, queue_size=10)
        self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback, queue_size=1)
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)
        self.cmd_vel = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=10)

        self.rate = rospy.Rate(50)
        start_time = rospy.Time.now()
        
        print("Waiting, the node is initializing...")
        while(len(self.scan.ranges)  == 0 ):
            time.sleep(1)

        # self.drive_straight(0.1, 0.1)
        print(len(self.scan.ranges))
        self.goalx = self.scan.ranges[len(self.scan.ranges)/2] - self.cspace
        goal_distance = self.goalx
        
        self.rotate_relative(90)
        time.sleep(1)
        self.rotate_relative(-90)
        time.sleep(1)
        self.rotate_relative(180)

        rospy.loginfo("Stopping Turtlebot")
        self.cmd_vel.publish(Twist())
    
    def radian2degree(self, radian):
        return radian*57.2958

    def degree2radian(self, degree):
        return degree/57.2958

    def face_to_goal(self):
        pass
    
    def set_linear_goal(self, x_goal, y_goal):
        pass

    def go_to_goal(self, x_goal, y_goal, end_time):
        pass

    def distance(self, x_goal, y_goal):
        return sqrt(((self.x - x_goal)**2)+((self.y - y_goal)**2))

    def scan_callback(self, scan_msg):
        # get an idea from this link about how to read the laser scanner
        self.scan = scan_msg
        # https://gist.github.com/atotto/c47bc69a48ed38e86947b5506b8e0e61

    def bumper_callback(self, bumper_msg):
        pass

    def odom_callback(self, odom_msg):
        # try to save x, y, and theta in your instance variables, for your own convience.
        self.odom = odom_msg

    def bug_angle():
        return atan2(goal.pose.pose.position.y - poser.pose.pose.position.y, goal.pose.pose.position.x- poser.pose.pose.position.x)

    def drive(self):
        start_sec = rospy.Time.now().to_sec()
        cur_sec = rospy.Time.now().to_sec()
        while (cur_sec - start_sec < 2.5):
            msg = Twist()
            msg.linear.x = 0.2
            self.cmd_vel.publish(msg)
            cur_sec = rospy.Time.now().to_sec()
            self.rate.sleep()

        twist_msg = Twist()
        self.cmd_vel.publish(twist_msg)

    def rotate(self, angle):
        twist_msg = Twist()
        twist_msg.angular.z = 1 if angle > 0 else -1
        angle = self.normalize_angle(angle)
        self.goal_rotation = abs(angle)

        rd = abs(self.rotation_distance())
        to_go = abs(rd - self.goal_rotation)
        print(to_go)

        while (True):
            print(to_go)
            rd = abs(self.rotation_distance())
            to_go = abs(rd - self.goal_rotation)
            self.cmd_vel.publish(twist_msg)
            self.rate.sleep()
            if to_go < 5:
                break

    def rotate_relative(self,angle):
        goal_rotation = angle + self.radian2degree(self.theta)

        if (goal_rotation > 180):
            goal_rotation = goal_rotation - 360

        if (goal_rotation < -180):
            goal_rotation = 360 + goal_rotation

        self.rotate(goal_rotation)

    def normalize_angle(self,angle):
        angle %= 360
        angle = (angle + 360) % 360
        if angle > 180:
            angle -= 360
        return angle

    def rotation_distance(self):
        """Calculate the difference in yaw between two quaternions"""
        (r1, p1, y1) = tf.transformations.euler_from_quaternion([self.odom.pose.pose.orientation.x,
                                                                 self.odom.pose.pose.orientation.y,
                                                                 self.odom.pose.pose.orientation.z,
                                                                 self.odom.pose.pose.orientation.w])
        (r2, p2, y2) = tf.transformations.euler_from_quaternion([self.starting_odom.pose.pose.orientation.x,
                                                                 self.starting_odom.pose.pose.orientation.y,
                                                                 self.starting_odom.pose.pose.orientation.z,
                                                                 self.starting_odom.pose.pose.orientation.w])
        # r, p, y now contain the roll, pitch and yaw from the two quaternions
        # you can derive the angle from that in a number of ways
        euler = abs(y2 - y1)
        yaw_degrees = euler * 180.0 / 3.14
        # yaw_degrees += 360.0 if (yaw_degrees < 0) else yaw_degrees
        # print yaw_degrees
        return yaw_degrees

if __name__ == '__main__':
    try:
        Bug2()
    except rospy.ROSInterruptException:
        pass

    