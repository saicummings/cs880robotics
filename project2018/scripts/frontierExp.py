#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 15:04:02 2017

@author: Mohit and Sai 
"""
import rospy
import numpy as np
import tf
from nav_msgs.msg import OccupancyGrid, MapMetaData, Odometry
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseStamped, Twist, Vector3, Point, Pose2D
from actionlib_msgs.msg import GoalStatus, GoalStatusArray

class Position:
    """ Helper class that describes the positon of the turtlebot:
            x,y and theta 
    """
    x = 0.0
    y = 0.0
    theta = 0.0
    
class RobotState():
    spin = 0
    move_base = 2
    
def normalize_angle(angle):
    """REDUCE ANGLES TO -pi pi"""
    angle %= np.pi*2
    if angle > np.pi:
        angle -= np.pi*2
    return angle


class Frontier_Based_Exploration():
        
    def costmap_callback(self, map_msg):
        """ Callback to handle Map messages. """

        self.meta.height = map_msg.info.height
        self.meta.width = map_msg.info.width
       
        self.meta.origin.position.x = map_msg.info.origin.position.x
        self.meta.origin.position.y = map_msg.info.origin.position.y

        self.origin.x = map_msg.info.origin.position.x
        self.origin.y = map_msg.info.origin.position.y

        self.ogrid_sizeX = map_msg.info.width
        self.ogrid_sizeY = map_msg.info.height

        self.current_map.data = map_msg.data

        self.grid_size = map_msg.info.resolution
        self.meta.resolution= map_msg.info.resolution



                
    def status_callback(self, status_msg):
        """ """
        if  len(status_msg.status_list) >0:
            for i in range(len(status_msg.status_list)):
                ID = int(status_msg.status_list[i].goal_id.id.split('-')[1])
                # Store the stautus of all WPTs
                if len(self.move_base_status) >= ID+1:
                    self.move_base_status[ID] = status_msg.status_list[i].status
                else:
                    self.move_base_status.append(status_msg.status_list[i].status)
                    
    def odom_callback(self, odom_msg):
        """ callback to handle odometry messages"""        
        # convert those into x/y/theta positions
        self.cur_odom.x = odom_msg.pose.pose.position.x
        self.cur_odom.y = odom_msg.pose.pose.position.y
        
        rot = tf.transformations.euler_from_quaternion([0.0, 0.0, odom_msg.pose.pose.orientation.z, odom_msg.pose.pose.orientation.w])
        self.cur_odom.theta = rot[2]
       

    def xy2grid(self, x,y):
        """ Converts the local X,Y coordinates to the grid coordinate system."""
        gridX = int(round((x-self.origin.x)/self.grid_size))
        gridY = int(round((y-self.origin.y)/self.grid_size))
        return gridX,gridY
        
    def grid2xy(self, gridX, gridY):
        """ Converts the grid coordinates to the local X,Y. """
        x = gridX*self.grid_size+self.origin.x
        y = gridY*self.grid_size+self.origin.y
        return x,y
        
    def xy2mapIndex(self, x,y):
        """ Converts the x,y grid coordinates into a row-major index for the 
            map. """

        if x>self.ogrid_sizeX or y>self.ogrid_sizeY:
            print ('MAP IS TOO SMALL')
            return self.ogrid_sizeX * self.ogrid_sizeY -1
            # return -1
        else:
            return int(y*self.ogrid_sizeY + x)
        
    def mapIndex2xy(self, index):
        """ Converts the row-major index for the map into x,y coordinates."""

        x_c = np.mod(index, self.ogrid_sizeY)
        y_c = (index-x_c)/self.ogrid_sizeY
        return x_c, y_c

    def distanceFomula(self, x1,y1, x2,y2):
        return np.sqrt(np.square(x2 - x1) + np.square(y2-y1))
         
    def calcCost(self, centroidX, centroidY, frontierLength):
        """ Calculate the cost of the frontier's centroid using a combo of
            the distance to the centroid and the length of the centroid.
        """
       
        
    def calcCost_dist(self, X, Y):
        """ Calculate the cost of the frontier's centroid using just the
            distance to the centroid. 
        """
        return self.distanceFomula(X, Y, self.position.x, self.position.y)
        
    def onFrontier(self, index):
        """ This function takes in an index of a cell in the map and determines
            if it is part of the frontier. The frontier is defined as regions
            on the border of empty space and unknown space.
        """
        x, y = self.mapIndex2xy(index)
        
        top = -1
        if index <= self.ogrid_sizeX:
            top = 0

        bottom = 2
        if index >= (self.ogrid_sizeX*self.ogrid_sizeY - self.ogrid_sizeX):
            bottom = 1

        left = -1
        if index % self.ogrid_sizeX == 0:
            left = 0

        right = 2
        if (index + 1) % self.ogrid_sizeX == 0:
            right = 1

        for r in range(top,bottom):
            for c in range(left,right):
                if r == 0 and c == 0:
                    continue
                if self.current_map.data[self.xy2mapIndex(x + r, y + c)] == -1:
                    return True

        return False

        
    def Frontier(self):
        """ This funtion finds the frontier on the map and returns a 1D vector 
            containing the indices of the cells on the frontier."""
        frontier = []
        for i in range(len(self.current_map.data)):
            #TODO Implement
            if self.current_map.data[i]<=30 and self.current_map.data[i]>=0:
                if self.onFrontier(i):
                    frontier.append(i)
        return frontier
        
    def blogDetection(self, frontier):
        """ This function runs the connected components algorithm on the 
            inputed frontier, which is a 1D vector containing the indices of 
            the cells on the frontier.


            ***Refenced algorithm from Frontier Based Exploration.pdf slide # 34

        """
        labels = np.zeros_like(frontier, dtype=np.uint16)
        full_labels = np.ones_like(self.current_map.data, dtype=np.uint16)*-1
        equiv = []
        cur_label = -1
        cntr = -1
        # Do first pass and label frontiers
        mapFront = {}
        label = -1
        for front in frontier:

            top, left, topR, topL = False, False, False, False
            indT, indTR, indL, indTL = -1, -1, -1, -1

            if (front >= self.ogrid_sizeX):
                indT = front - self.ogrid_sizeX
                if full_labels[indT] != -1:
                    top = True

                
             # Check top Right

                if front % self.ogrid_sizeX != self.ogrid_sizeX-1:
                    indTR = front - self.ogrid_sizeX + 1
                    if full_labels[indTR] != -1:
                        topR = True

            if front % self.ogrid_sizeX != 0:
                indL = front - 1
                if full_labels[indL] !=-1:
                    left = True

                # Check top Left

                if (front >= self.ogrid_sizeX):
                    indTL = front - self.ogrid_sizeX-1
                    if full_labels[indTL] !=-1:
                        topL = True


            if not top and not left and not topR and not topL:
                cur_label +=1
                label = cur_label
            else:
                if top:
                    label = full_labels[indT]
                elif left:
                    label = full_labels[indL]
                elif topL:
                    label = full_labels[indTL]
                elif topR:
                    label = full_labels[indTR]

            if label not in mapFront:
                mapFront[label] = []
            mapFront[label].append(front)


            full_labels[front] = label  

        return mapFront
        
    
    def getFrontier(self):
        """ This function defines and labels the frontier. If the froniter is 
            smaller than a meter it is removed.""" 
        unlabeledFrontier = self.Frontier()
        #labels, equiv = self.blogDetection(unlabeledFrontier)        
        
        m = self.blogDetection(unlabeledFrontier) 
        
        #print(self.current_map.data)

        

        # Remove all frontiers smaller than 100 cm

        for key, value in m.items():
            if len(value) <= 20:
                del m[key]
        #print(len(m))
        #print(m)
        self.mapp = m
        return m


        
    def calc_centroid(self, points):
        """ This function takes in a set of points and finds its centroid.
            
            Input:
                points - 2D array where the each row contains the X,Y location 
                    of the frontier cell
        """
        size = len(points)
        xSum = 0
        ySum = 0 

        for p in points:
            x, y = self.mapIndex2xy(p)
            xSum += x
            ySum += y

        x_c = int(xSum/size)
        y_c = int(ySum/size)
        
        return self.grid2xy(x_c, y_c)
    
    def pickBestCentroid(self, frontiers):
        """ Takes in all frontiers (as a 3D array) and choses the best frontier"""
        self.centroidX = []
        self.centroidY = []
        self.centroidIndex = []
        self.cost = []
        
        centroid_index = 0

        #IMPLEMENT

        bestKey = next(iter(frontiers))

        for key, value in frontiers.items(): 
            if len(value) > len(frontiers[bestKey]):
                bestKey = key
        #print(bestKey)
        #return self.bestCentroid()

        l = frontiers[bestKey]
        del frontiers[bestKey]

        centX, centY = self.calc_centroid(l)
        
        return centX, centY
            
    def updateBestCentoid(self):
        """ """

        
    def bestCentroid(self):
        """ This function takes the precalculated x/y and cost values of the 
            centroid and picks the returns the index to the cell that has the minimum cost"""
            
        
        
    def makeMarker(self, centroidX, centroidY, ID, action = Marker.ADD,  new=True):
        """ Creates a marker on RVIZ for the centroid of the frontier"""
        

            
    def makelineMarker(self, XY, ID, action = Marker.ADD, new=True):
        """ Creates a line marker on RVIZ for the entire frontier"""

            
    def removeAllMarkers(self):
        """This function removes all markers from rviz"""

        
    def pose_callback(self, pose_msg):
        """ callback to handle odometry messages"""        
        self.position.time = rospy.get_rostime().to_sec()
        self.position.x = pose_msg.x
        self.position.y = pose_msg.y
        self.position.theta = pose_msg.theta
            
        # print "x: " + str(self.position.x) + " y: " + str(self.position.y) + " theta: " + str(self.position.theta)

    
    def newPose(self, x, y, rot_z=0.1,rot_w=0.1):
        """Takes in the new waypoint and sets the new goal position"""        
        new = PoseStamped()
        new.header.seq=1
        new.header.stamp.secs = rospy.get_rostime().to_sec()
        
        new.header.frame_id= 'map'
        
        # Positions in the map
        
        new.pose.position.x = x
        new.pose.position.y = y
        new.pose.position.z = 0.0
        
        new.pose.orientation.x = 0.0
        new.pose.orientation.y = 0.0
        new.pose.orientation.z = rot_z
        new.pose.orientation.w = rot_w
        
        # Publish the new position
        self.cmd_pose.publish(new)
    
    def angle_traveled(self):
        """ Calculates the angle traveled between the current and previous 
            angle.
        """
       
    def spin360(self):
        """ Spins the robot 360 degrees w.r.t the odometer""" 
        self.angle_trav = 0
        self.done_spinning = False
        while(self.angle_trav < np.pi*2):
            rot = self.cur_odom.theta-self.prev_odom.theta
            self.prev_odom.theta =  self.cur_odom.theta 

            if abs(rot) > np.pi:
                rot = abs(rot) - 2*np.pi

            self.angle_trav = self.angle_trav + rot

            self.cmd_vel.publish(Twist(Vector3(0,0,0),Vector3(0,0,0.25)))

        self.done_spinning = True
        self.cmd_vel.publish(Twist())



    def move2frontier(self,X, Y):
        """ Navigate to the centroid of the chosen frontier"""
        print 'Navigating to', X , Y
        self.newPose(X, Y)


    def run(self):
        """ Runs the frontier based exploration. """
        start = rospy.Time.now().to_sec()
        while (rospy.Time.now().to_sec()-start) < 1:
            self.r.sleep()
        
        print 'Spinning'
        self.spin360()
        mm = self.getFrontier()

        while self.done_spinning == False:
            self.start_spin = True

        centroidX, centroidY = self.pickBestCentroid(mm)
        '''
        print(centroidX, centroidY)
        self.move2frontier(centroidX, centroidY)

        
        while not rospy.is_shutdown() and len(self.mapp) > 0: # removed timing info
            try:
                # self.get_current_position()
                # If the robot is currently spinning, check if it completed a 
                #   rotation and if it has, find the frontiers.
                if self.robotState == RobotState.spin:
                    self.spin360()
                    mm = self.getFrontier()

                    centroidX, centroidY = self.pickBestCentroid(mm)

                    self.robotState = RobotState.move_base
                
                
                if self.robotState == RobotState.move_base:
                    self.move2frontier(centroidX, centroidY)
                    self.robotState = RobotState.spin


                print(self.robotState)
            except KeyboardInterrupt:
                print 'Exiting' 
            
            
        # At the end remove all rviz markers
        self.removeAllMarkers()
        for i in range(5):
            self.r.sleep()

        '''
        print 'exiting'
        
    def __init__(self):
        """ Initialize """
        rospy.init_node('Frontier_exploration')
        
        self.listener = tf.TransformListener()        
        
        self.mapp = {}

        # Get the parameters for the grid
        self.ogrid_sizeX = rospy.get_param('x_size', 500)
        self.ogrid_sizeY = rospy.get_param('y_size', 500)
        self.grid_size = rospy.get_param('grid_size', 0.05) # in meters/cell (5cm)
        
        # Sensor Meta data
        self.min_range = rospy.get_param('max_range',0.4)
        self.max_range = rospy.get_param('max_range',6.0)
        
        # reliability
        self.p_measurement_given_occupied = rospy.get_param('p_z|occ',0.9)
        self.p_measurement_given_notOccupied = rospy.get_param('p_z|notOcc',0.3)
        
        # Initialize some varables to store the objects to be published/subscribed
        self.position = Position()
        self.robotState = RobotState()
        self.frontierCentroid = Position()
        self.markerArray = MarkerArray()
        self.lineMarker = MarkerArray()
        self.cur_odom = Position()
        self.prev_odom = Position()
        
        self.current_map = OccupancyGrid()
        self.meta = MapMetaData()
        self.new_pose = PoseStamped()
        
        # Rotation Booleans
        self.start_spin = True
        self.done_spinning = True
        self.new_cmd = False
        
        self.unreachable_frontiers = []
        self.reached_centroids=[]
        self.WPT_ID = 0
        self.angle_trav = 0
        self.move_base_status = [0]
        
        self.origin = Position()
        
        self.r = rospy.Rate(50)
        
        # publishers for OccupancyGrid and move base messages
        self.marker_pub = rospy.Publisher('/centroid_marker', MarkerArray, queue_size=100)
        self.lineMarker_pub = rospy.Publisher('/frontier_marker', MarkerArray, queue_size=100)
        self.cmd_pose = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=100)
        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=100)
        
        # subscribers for odometry and position (/move_base/feedback) messages
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.odom_callback)
        self.sub_map = rospy.Subscriber('/frontier_map', OccupancyGrid, self.costmap_callback)
        self.sub_status = rospy.Subscriber('/move_base/status', GoalStatusArray, self.status_callback)
        self.base_pos_sub = rospy.Subscriber("/pose2D", Pose2D, self.pose_callback)
                
        self.run()
        
        return

if  __name__=="__main__":
    l = Frontier_Based_Exploration()

