#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue May  2 17:07:03 2017

@author: teamxxx
"""
import rospy
import numpy as np
import tf
import math
import time
from nav_msgs.msg import OccupancyGrid, MapMetaData
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist, Pose2D

class Position:
    """ Helper class that describes the positon of the turtlebot:
            x,y and theta 
    """
    x = 0.0
    y = 0.0
    theta = 0.0
    time = 0.0
    
    
def normalize_angle(angle):
    """REDUCE ANGLES TO -pi pi"""
    angle %= np.pi*2
    if angle > np.pi:
        angle -= np.pi*2
    return angle
    
class MakeMap():
    def vel_callback(self, vel_msg):
        """ """

    def scan_callback(self, scan_msg):
        """ Callback for the /scan messages."""
        self.scan = scan_msg

    
    def xy2grid(self, x,y):
        """ Converts the local X,Y coordinates to the grid coordinate system."""
        gridX = int(round((x-self.origin.x)/self.grid_size))
        gridY = int(round((y-self.origin.y)/self.grid_size))
        return gridX,gridY
        
    def grid2xy(self, gridX, gridY):
        """ Converts the grid coordinates to the local X,Y. """
        x = gridX*self.grid_size+self.origin.x
        y = gridY*self.grid_size+self.origin.y
        return x,y
        
    def xy2mapIndex(self, x,y):
        """ Converts the x,y grid coordinates into a row-major index for the 
            map. """
        if x>self.ogrid_sizeX or y>self.ogrid_sizeY:
            print 'MAP IS TOO SMALL!!!'
            return self.ogrid_sizeX * self.ogrid_sizeY -1
        else:
            return int(y*self.ogrid_sizeY + x)
        
        
    def mapIndex2xy(self, index):
        """ Converts the row-major index for the map into x,y coordinates."""
        x = np.mod(index, self.ogrid_sizeY)
        y = (index-x)/self.ogrid_sizeY
        return x,y
         
    def setOrigin(self):
        """ Sets the origin. """
        

        
    def setOccupancy(self, index, prior, Free):
        """ This function defines the occupancy of the cells."""
        # If there is no previous data, then use a prior of 0.5 (Beta distribution)

    
    def updateNeighbors(self, scanX, scanY):
        """ Update the free occupancy grid cells between the robot and the   
            obstacle found using the laser scan.
        """
        cell = self.xy2mapIndex( scanX, scanY)
        self.current_map.data[cell] = 100



    def updateMap(self):
        """ This function updates the occupancy grid cells with the current 
            scan. """    

        
        angle_min = self.scan.angle_min # start angle of the scan [rad] 
        angle_max = self.scan.angle_max # end angle of the scan [rad]
        angle_increment = self.scan.angle_increment # angular distance between measurements [rad]
        
        #print(angle_min, angle_max)

        #increments = len(self.scan.ranges)
        inc = 0
        obs_hit = True

        cell_updated = {}
        for r in self.scan.ranges:
            if not math.isnan(r) and r >= self.min_range and r <= self.max_range:
                    
                angle = angle_min + inc * angle_increment + self.position.theta
                obs_x = self.position.x + r * math.cos(angle)
                obs_y = self.position.y + r * math.sin(angle)
                (obs_gridx, obs_gridy) = self.xy2grid(obs_x, obs_y)
                (bot_gridx, bot_gridy) = self.xy2grid(self.position.x,self.position.y)
                obs_gridx += self.init_originx
                obs_gridy += self.init_originy
                bot_gridx += self.init_originx
                bot_gridy += self.init_originy

                # Cells is a list of cells from the robot to the obstacle. 
                # This is form making the cells between the obstacle and the robot unoccupied 
                cells = self.bresenham(bot_gridx, bot_gridy, obs_gridx, obs_gridy )

                c_inc = 0
                for c in cells:
                    cell_rowmajor = self.xy2mapIndex(c[0] , c[1])

                    if cell_rowmajor not in cell_updated:
                        # continue # Not sure if we should ignore cells that has already been updated in this sweep.
                        if c_inc is len(cells) - 1:
                            # Current c is the obstacle.         
                            self.current_map.data[cell_rowmajor] = 90
                            cell_updated[cell_rowmajor] = 90   
                        elif self.current_map.data[cell_rowmajor] is -1:
                            self.current_map.data[cell_rowmajor] = 30
                        else:
                            prior = self.current_map.data[cell_rowmajor]
                            bayes = (prior * 50 / (prior * 50 + 20 * 50)) * prior
                            self.current_map.data[cell_rowmajor] = bayes
                            cell_updated[cell_rowmajor] = bayes
                    c_inc += 1

                # Current location of bot
                # self.current_map.data[self.xy2mapIndex(bot_gridx, bot_gridy)] = 90


            inc += 1
        #print(inc * angle_increment)     

        self.pub_map.publish(self.current_map)

        
    def publishNewMap(self):
        """ This function publishes an occupancy grid to the ROS topic 
            /frontie_map."""
        self.pub_map.publish(self.current_map)

    def bresenham(self, x1, y1, x2, y2):
        '''
        http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm#Python
        '''
        dx = x2 - x1
        dy = y2 - y1
    
        # Determine how steep the line is
        is_steep = abs(dy) > abs(dx)
    
        # Rotate line
        if is_steep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
    
        # Swap start and end points if necessary and store swap state
        swapped = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            swapped = True
    
        # Recalculate differentials
        dx = x2 - x1
        dy = y2 - y1
    
        # Calculate error
        error = int(dx / 2.0)
        ystep = 1 if y1 < y2 else -1
    
        # Iterate over bounding box generating points between start and end
        y = y1
        points = []
        for x in range(x1, x2 + 1):
            coord = (y, x) if is_steep else (x, y)
            points.append(coord)
            error -= abs(dy)
            if error < 0:
                y += ystep
                error += dx
    
        # Reverse the list if the coordinates were swapped
        if swapped:
            points.reverse()
        return points


    def pose_callback(self, pose_msg):
        """ callback to handle odometry messages"""        

    def __init__(self):
        print('starting make map program')
        """ Initialize """
        rospy.init_node('Make_Map')
        
        self.listener = tf.TransformListener()        
        # rospy.wait_for_service('spawn')
        
        self.init_gridx = 500
        self.init_gridy = 500
        self.init_gridsize = 0.05
        self.init_originx = 250
        self.init_originy = 250

        # Get the parameters for the grid
        self.ogrid_sizeX = rospy.get_param('x_size', self.init_gridx)
        self.ogrid_sizeY = rospy.get_param('y_size', self.init_gridy)
        self.grid_size = rospy.get_param('grid_size', self.init_gridsize) # in meters/cell (5cm)
        
        # Sensor Meta data
        self.min_range = rospy.get_param('max_range',0.5)
        self.max_range = rospy.get_param('max_range',6.0)
        
        # reliability
        self.p_measurement_given_occupied = rospy.get_param('p_z|occ',0.9)
        self.p_measurement_given_notOccupied = rospy.get_param('p_z|notOcc',0.3)
        
        # Initialize some varables to store the objects to be published/subscribed
        self.position = Position()
        self.camera_pos= Position()
        
        self.velocity = Twist()
        
        # map
        self.current_map = OccupancyGrid()
        self.meta = MapMetaData()
        self.scan = LaserScan()
        
        # Rotation Booleans
        self.start_spin = True
        self.done_spinning = True
        self.new_cmd = False
        
        # Initialize the map as unknown (-1)
        self.current_map.data = [-1]*self.ogrid_sizeX*self.ogrid_sizeY # row-major order

        # Initialized info [Sai]
        self.current_map.info.width = self.init_gridx
        self.current_map.info.height = self.init_gridy
        self.current_map.info.resolution = self.init_gridsize
        self.current_map.info.origin.position.x = -self.init_gridx * self.init_gridsize / 2
        self.current_map.info.origin.position.y = -self.init_gridy * self.init_gridsize / 2
        self.position.x = 0
        self.position.y = 0
        
        self.scan_endpoints = []
        
        self.r = rospy.Rate(50)
        rospy.sleep(2)

        
        # publishers for OccupancyGrid
        self.pub_map = rospy.Publisher('/frontier_map', OccupancyGrid, queue_size=100)
        
        # subscribers for odometry and position (/move_base/feedback) messages
        self.cmd_vel = rospy.Subscriber('/cmd_vel_mux/input/navi', Twist, self.vel_callback)
        self.scan_sub = rospy.Subscriber("/scan", LaserScan, self.scan_callback)
        # self.sub_map = rospy.Subscriber('/map', OccupancyGrid, self.costmap_callback)
        self.base_pos_sub = rospy.Subscriber("/pose2D", Pose2D, self.pose_callback)
        
        self.listener.waitForTransform('/odom', '/base_footprint', rospy.Time(0), rospy.Duration(1))
        
        # print(self.current_map)

        print("Waiting, the scan node is initializing...")

        while(len(self.scan.ranges)	== 0 ):
			continue

         # self.origin = self.setOrigin()
        (trans, rot) = self.listener.lookupTransform('/odom', '/base_footprint', rospy.Time(0))
        p = Position()
        p.x = 0
        p.y = 0
        euler = tf.transformations.euler_from_quaternion(rot)
        p.theta = euler[2]
        self.origin = p

        print('Starting while loop')

        self.pub_map.publish(self.current_map)

        while (not rospy.is_shutdown()):

            try:
                # self.current_map.data = [-1]*self.ogrid_sizeX*self.ogrid_sizeY # row-major order

                (trans, rot) = self.listener.lookupTransform('/odom', '/base_footprint', rospy.Time(0))
                #print(trans)
                #self.publishNewMap()
               
                self.position.x = trans[0]
                self.position.y = trans[1]
                #euler = tf.transformations.euler_from_quaternion(rot)
                self.position.theta = tf.transformations.euler_from_quaternion(rot)[2]
                #print(euler[2] * 57.2958)
                #(gridx, gridy) = self.xy2grid(self.position.x,self.position.y)

                self.updateMap()
            
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                print("exception" )                
                continue

            

            
            self.r.sleep()
        
        return
if __name__ == "__main__":
    m = MakeMap()



"""
rosrun reed_final makeMap.py 
roslaunch reed_final map_maker.launch 
roslaunch reed_final rviz.launch 
roslaunch turtlebot_teleop keyboard_teleop.launch 
roslaunch turtlebot_gazebo turtlebot_world.launch 


http://wiki.ros.org/laser_scan_matcher
"""


