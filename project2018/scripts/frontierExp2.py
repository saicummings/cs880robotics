#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 15:04:02 2017

@author: Mohit and Sai 
"""
import rospy
import numpy as np
import tf
from nav_msgs.msg import OccupancyGrid, MapMetaData, Odometry
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseStamped, Twist, Vector3, Point, Pose2D
from actionlib_msgs.msg import GoalStatus, GoalStatusArray

class Position:
    """ Helper class that describes the positon of the turtlebot:
            x,y and theta 
    """
    x = 0.0
    y = 0.0
    theta = 0.0
    
class RobotState():
    spin = 0
    move_base = 2
    
def normalize_angle(angle):
    """REDUCE ANGLES TO -pi pi"""
    angle %= np.pi*2
    if angle > np.pi:
        angle -= np.pi*2
    return angle


class Frontier_Based_Exploration():
        
    def costmap_callback(self, map_msg):
        """ Callback to handle Map messages. """

        self.meta.height = map_msg.info.height
        self.meta.width = map_msg.info.width
       
        self.meta.origin.position.x = map_msg.info.origin.position.x
        self.meta.origin.position.y = map_msg.info.origin.position.y

        self.origin.x = map_msg.info.origin.position.x
        self.origin.y = map_msg.info.origin.position.y

        self.ogrid_sizeX = map_msg.info.width
        self.ogrid_sizeY = map_msg.info.height

        self.current_map.data = map_msg.data

        self.grid_size = map_msg.info.resolution
        self.meta.resolution= map_msg.info.resolution


    def rotation_distance(self, q1, q2):
        """ Calculate the difference in yaw between two quaternions. For the 
            turtlebot, the z and w are the only two quaternions.

            Inputs:
                q1 - self.odom.pose.pose.orientation.z
                q2 - self.odom.pose.pose.orientation.w
            
            Ouput:
                yaw - yaw of the turtlebot
        """
        # This should convert the quaternions to roll/pitch/yaw. We only have 
        #   quaternions z and w so the output is only yaw.
        rot = tf.transformations.euler_from_quaternion([0.0, 0.0, q1, q2])
        yaw = rot[2]
        return yaw
                
    def status_callback(self, status_msg):
        """ """
        if  len(status_msg.status_list) >0:
            for i in range(len(status_msg.status_list)):
                ID = int(status_msg.status_list[i].goal_id.id.split('-')[1])
                # Store the stautus of all WPTs
                if len(self.move_base_status) >= ID+1:
                    self.move_base_status[ID] = status_msg.status_list[i].status
                else:
                    self.move_base_status.append(status_msg.status_list[i].status)
                    
    def odom_callback(self, odom_msg):
        """ callback to handle odometry messages"""        
        # convert those into x/y/theta positions
        self.cur_odom.x = odom_msg.pose.pose.position.x
        self.cur_odom.y = odom_msg.pose.pose.position.y
        
        rot = tf.transformations.euler_from_quaternion([0.0, 0.0, odom_msg.pose.pose.orientation.z, odom_msg.pose.pose.orientation.w])
        self.cur_odom.theta = rot[2]
       
    def get_current_position(self):
        """ Callback to get the current position"""
        trans, rot = self.listener.lookupTransform('map', 'base_link', rospy.Time(0))
        self.position.x= trans[0]
        self.position.y = trans[1]
        self.position.theta= normalize_angle(self.rotation_distance(rot[2],rot[3]))

    def xy2grid(self, x,y):
        """ Converts the local X,Y coordinates to the grid coordinate system."""
        gridX = int(round((x-self.origin.x)/self.grid_size))
        gridY = int(round((y-self.origin.y)/self.grid_size))
        return gridX,gridY
        
    def grid2xy(self, gridX, gridY):
        """ Converts the grid coordinates to the local X,Y. """
        x = gridX*self.grid_size+self.origin.x
        y = gridY*self.grid_size+self.origin.y
        return x,y
        
    def xy2mapIndex(self, x,y):
        """ Converts the x,y grid coordinates into a row-major index for the 
            map. """

        if x>self.ogrid_sizeX or y>self.ogrid_sizeY:
            print ('MAP IS TOO SMALL')
            return self.ogrid_sizeX * self.ogrid_sizeY -1
            # return -1
        else:
            return int(y*self.ogrid_sizeY + x)
        
    def mapIndex2xy(self, index):
        """ Converts the row-major index for the map into x,y coordinates."""

        x_c = np.mod(index, self.ogrid_sizeY)
        y_c = (index-x_c)/self.ogrid_sizeY
        return x_c, y_c

    def distanceFomula(self, x1,y1, x2,y2):
        return np.sqrt(np.square(x2 - x1) + np.square(y2-y1))
         
    def calcCost(self, centroidX, centroidY, frontierLength):
        """ Calculate the cost of the frontier's centroid using a combo of
            the distance to the centroid and the length of the centroid.
        """
       
        
    def calcCost_dist(self, X, Y):
        """ Calculate the cost of the frontier's centroid using just the
            distance to the centroid. 
        """
        return self.distanceFomula(X, Y, self.position.x, self.position.y)
        
    def onFrontier(self, index):
        """ This function takes in an index of a cell in the map and determines
            if it is part of the frontier. The frontier is defined as regions
            on the border of empty space and unknown space.
        """
        x, y = self.mapIndex2xy(index)
        
        top = -1
        if index <= self.ogrid_sizeX:
            top = 0

        bottom = 2
        if index >= (self.ogrid_sizeX*self.ogrid_sizeY - self.ogrid_sizeX):
            bottom = 1

        left = -1
        if index % self.ogrid_sizeX == 0:
            left = 0

        right = 2
        if (index + 1) % self.ogrid_sizeX == 0:
            right = 1

        for r in range(top,bottom):
            for c in range(left,right):
                if r == 0 and c == 0:
                    continue
                if self.current_map.data[self.xy2mapIndex(x + r, y + c)] == -1:
                    return True

        return False

        
    def Frontier(self):
        """ This funtion finds the frontier on the map and returns a 1D vector 
            containing the indices of the cells on the frontier."""
        frontier = []
        for i in range(len(self.current_map.data)):
            #TODO Implement
            if self.current_map.data[i]<=30 and self.current_map.data[i]>=0:
                if self.onFrontier(i):
                    frontier.append(i)
        return frontier
        
    def blogDetection(self, frontier):
        """ This function runs the connected components algorithm on the 
            inputed frontier, which is a 1D vector containing the indices of 
            the cells on the frontier.


            ***Refenced algorithm from Frontier Based Exploration.pdf slide # 34

        """
        labels = np.zeros_like(frontier, dtype=np.uint16)
        full_labels = np.ones_like(self.current_map.data, dtype=np.uint16)*-1
        equiv = []
        cur_label = -1
        cntr = -1
        # Do first pass and label frontiers
        mapFront = {}
        label = -1
        for front in frontier:

            top, left, topR, topL = False, False, False, False
            indT, indTR, indL, indTL = -1, -1, -1, -1

            if (front >= self.ogrid_sizeX):
                indT = front - self.ogrid_sizeX
                if full_labels[indT] != -1:
                    top = True

                
             # Check top Right

                if front % self.ogrid_sizeX != self.ogrid_sizeX-1:
                    indTR = front - self.ogrid_sizeX + 1
                    if full_labels[indTR] != -1:
                        topR = True

            if front % self.ogrid_sizeX != 0:
                indL = front - 1
                if full_labels[indL] !=-1:
                    left = True

                # Check top Left

                if (front >= self.ogrid_sizeX):
                    indTL = front - self.ogrid_sizeX-1
                    if full_labels[indTL] !=-1:
                        topL = True


            if not top and not left and not topR and not topL:
                cur_label +=1
                label = cur_label
            else:
                if top:
                    label = full_labels[indT]
                elif left:
                    label = full_labels[indL]
                elif topL:
                    label = full_labels[indTL]
                elif topR:
                    label = full_labels[indTR]

            if label not in mapFront:
                mapFront[label] = []
            mapFront[label].append(front)


            full_labels[front] = label  

        return mapFront
        
    
    def getFrontier(self):
        """ This function defines and labels the frontier. If the froniter is 
            smaller than a meter it is removed.""" 
        unlabeledFrontier = self.Frontier()
        #labels, equiv = self.blogDetection(unlabeledFrontier)        
        
        m = self.blogDetection(unlabeledFrontier) 
        
        #print(self.current_map.data)

        

        # Remove all frontiers smaller than 100 cm

        for key, value in m.items():
            if len(value) <= 20:
                del m[key]
        #print(len(m))
        #print(m)
        self.mapp = m
        return m


        
    def calc_centroid(self, points):
        """ This function takes in a set of points and finds its centroid.
            
            Input:
                points - 2D array where the each row contains the X,Y location 
                    of the frontier cell
        """
        if len(points) == 0:
            return
        num_Rows = len(points)
        x_c, y_c = np.sum(points, axis=0)
        x_c /= int(np.round(1.0*num_Rows))
        y_c /= int(np.round(1.0*num_Rows))
        
        x,y = self.grid2xy(x_c, y_c)
        return x, y, self.calcCost_dist(x,y)
    
    def pickBestCentroid(self, frontiers):
        """ Takes in all frontiers (as a 3D array) and choses the best frontier"""
        self.centroidX = []
        self.centroidY = []
        self.centroidIndex = []
        self.cost = []
        
        centroid_index = 0

        #IMPLEMENT

        bestKey = next(iter(frontiers))

        for key, value in frontiers.items(): 
            x_c, y_c, cost_c = self.calc_centroid(value)
            # Store centroid if it is known and 
            index = self.xy2mapIndex(x_c, y_c)
            if self.current_map.data[index]< 50:
                if [x_c, y_c] in self.unreachable_frontiers:
                    self.makelineMarker(value, centroid_index, new=False)
                    self.makeMarker(x_c, y_c, centroid_index, new=False)
                elif [x_c, y_c] not in self.reached_centroids:
                    self.makelineMarker(value, centroid_index)
                    self.makeMarker(x_c, y_c, centroid_index)
                    self.centroidX.append(x_c)
                    self.centroidY.append(y_c)
                    self.cost.append(cost_c)
                    self.centroidIndex.append(centroid_index)
                    centroid_index += 1
                
        return self.bestCentroid()
            
    def updateBestCentoid(self):
        """ """
        for i in range(len(self.centroidX)):
            # Update the cost
            self.cost[i] = (self.calcCost_dist(self.centroidX[i], self.centroidY[i]))
            
        return self.bestCentroid()
        
    def bestCentroid(self):
        """ This function takes the precalculated x/y and cost values of the 
            centroid and picks the returns the index to the cell that has the minimum cost"""
        if len(self.centroidX)>0:
            # Determine which centroid is the closest 
            index = np.argmin(self.cost)
            marker_index = self.centroidIndex[index]
            
            # Mark the centroid to navigate to as green
            self.markerArray.markers[marker_index].color.r = 0.0
            self.markerArray.markers[marker_index].color.g = 1.0
            self.markerArray.markers[marker_index].color.b = 0.0
            
            # Mark the frontier to navigate to as green
            self.lineMarker.markers[marker_index].color.r = 0.0
            self.lineMarker.markers[marker_index].color.g = 1.0
            self.lineMarker.markers[marker_index].color.b = 0.0
            
            # Publish the markerArray 
            self.marker_pub.publish(self.markerArray)
            self.lineMarker_pub.publish(self.lineMarker)
                
            print 'Navigating to', self.centroidX[index], self.centroidY[index], index
            
            return index
        else:
            print '\tNo Valid centroids'
            return -1
        
        
    def makeMarker(self, centroidX, centroidY, ID, action = Marker.ADD,  new=True):
        """ Creates a marker on RVIZ for the centroid of the frontier"""
        mark = Marker()
        mark.header.frame_id = '/map'
        mark.header.stamp = rospy.Time(0)
        mark.ns = 'Frontier'
        mark.id = ID
        mark.type = Marker.CUBE
        mark.action = action
        # Position with respect to the frame
        mark.pose.position.x = centroidX
        mark.pose.position.y = centroidY
        mark.pose.position.z = 0.0
        mark.pose.orientation.x = 0.0
        mark.pose.orientation.y = 0.0
        mark.pose.orientation.z = 0.0
        mark.pose.orientation.w = 1.0
        mark.scale.x = 0.3
        mark.scale.y = 0.3
        mark.scale.z = 0.3
        mark.color.a = 1.0
        mark.color.g = 0.0
        if new:
            mark.color.r = 0.0
            mark.color.b = 1.0
        else:
            mark.color.r = 1.0
            mark.color.b = 0.0
        if len(self.markerArray.markers) <= ID:
            self.markerArray.markers.append(mark)
        else:
            self.markerArray.markers[ID] = mark

            
    def makelineMarker(self, XY, ID, action = Marker.ADD, new=True):
        """ Creates a line marker on RVIZ for the entire frontier"""
        mark = Marker()
        mark.header.frame_id = '/map'
        mark.header.stamp = rospy.Time(0)
        mark.ns = 'Frontier'
        mark.id = ID
        mark.type = Marker.POINTS
        mark.action = action
        mark.scale.x = 0.05
        mark.scale.y = 0.05
        mark.color.a = 1.0
        mark.color.g = 0.0
        if new:
            mark.color.r = 0.0
            mark.color.b = 1.0
        else:
            mark.color.r = 1.0
            mark.color.b = 0.0
        for i in range(len(XY)):
            pnt = Point()
            x,y = self.grid2xy(XY[i][0], XY[i][1])
            pnt.x = x
            pnt.y = y
            pnt.z = 0.0
            mark.points.append(pnt)
        if len(self.lineMarker.markers) <= ID:
            self.lineMarker.markers.append(mark)
        else:
            self.lineMarker.markers[ID] = mark
            
    def removeAllMarkers(self):
        """This function removes all markers from rviz"""
        for i in range(len(self.markerArray.markers)):
            self.markerArray.markers[i].action = Marker.DELETE
            self.lineMarker.markers[i].action = Marker.DELETE
            
        self.marker_pub.publish(self.markerArray)
        self.lineMarker_pub.publish(self.lineMarker)
        
        self.markerArray = MarkerArray()
        self.lineMarker = MarkerArray()
        
    def pose_callback(self, pose_msg):
        """ callback to handle odometry messages"""        
        self.position.time = rospy.get_rostime().to_sec()
        self.position.x = pose_msg.x
        self.position.y = pose_msg.y
        self.position.theta = pose_msg.theta
            
        # print "x: " + str(self.position.x) + " y: " + str(self.position.y) + " theta: " + str(self.position.theta)

    
    def newPose(self, x, y, rot_z=0.1,rot_w=0.1):
        """Takes in the new waypoint and sets the new goal position"""        
        new = PoseStamped()
        new.header.seq=1
        new.header.stamp.secs = rospy.get_rostime().to_sec()
        
        new.header.frame_id= 'map'
        
        # Positions in the map
        
        new.pose.position.x = x
        new.pose.position.y = y
        new.pose.position.z = 0.0
        
        new.pose.orientation.x = 0.0
        new.pose.orientation.y = 0.0
        new.pose.orientation.z = rot_z
        new.pose.orientation.w = rot_w
        
        # Publish the new position
        self.cmd_pose.publish(new)
    
    def angle_traveled(self):
        """ Calculates the angle traveled between the current and previous 
            angle.
        """
        diff = self.cur_odom.theta-self.prev_odom.theta
        # If there is a big jump, then it must have crossed the -180/180 
        #  boundary.
        if abs(diff)>np.pi:
            diff = abs(diff) - 2*np.pi
            np.sign(diff) == -1
            diff *= -1
        
        self.angle_trav += diff
        
    def spin360(self):
        """ Spins the robot 360 degrees w.r.t the odometer"""            
        self.angle_traveled() 
        
        if self.start_spin:
            self.removeAllMarkers()
            self.robotState = RobotState.spin
            self.angle_trav = 0
            self.start_spin = False
            self.done_spinning = False
            
        omega = .75 # Rad/sec
            
        if (self.angle_trav < np.pi*2):
            # Write a new twist message
            self.cmd_vel.publish(Twist(Vector3(0,0,0),Vector3(0,0,omega)))
        else:
            # turn off Twist
            self.cmd_vel.publish(Twist())
            self.done_spinning = True
        
        # Store previous odometery
        self.prev_odom.x =  self.cur_odom.x 
        self.prev_odom.y =  self.cur_odom.y
        self.prev_odom.theta =  self.cur_odom.theta 



    def move2frontier(self,X, Y):
        """ Navigate to the centroid of the chosen frontier"""
        print 'Navigating to', X , Y
        self.newPose(X, Y)
        self.robotState = RobotState.move_base


    def run(self):
        """ Runs the frontier based exploration. """
        start = rospy.Time.now().to_sec()
        while (rospy.Time.now().to_sec()-start) < 1:
            self.r.sleep()
        
        self.start_spin = True
        print 'Spinning'
        self.spin360()

        while not rospy.is_shutdown():
            print('bbb')
            self.get_current_position()
            # If the robot is currently spinning, check if it completed a 
            #   rotation and if it has, find the frontiers.
            if self.robotState == RobotState.spin:
                if self.done_spinning:
                    self.robotState = RobotState.move_base
                    self.current_failed = []               
                    self.WPT_ID +=1
                    frontiers = self.getFrontier()
                    if len(frontiers) == 0:
                        self.removeAllMarkers()
                        return
                    self.frontier_index = self.pickBestCentroid(frontiers)
                    if self.frontier_index == -1:
                        self.removeAllMarkers()
                        print '\tNo more frontiers'
                        return
                    self.move2frontier(self.centroidX[self.frontier_index], self.centroidY[self.frontier_index])
                else:
                    if self.new_cmd:
                        self.start_spin=True
                        self.new_cmd = False
                    self.spin360()
                    
            # If the robot is trying to navigate to the centroid of a 
            #   frontier, check if the robot is in an end state
            if self.robotState == RobotState.move_base:
                # If it could not reach the centroid, then try the next 
                #   best one
                if len(self.move_base_status) > self.WPT_ID:
                    if self.move_base_status[self.WPT_ID] in [GoalStatus.ABORTED, GoalStatus.REJECTED]:
                        print '\tFailed to reach frontier. Trying next one.'
                        self.unreachable_frontiers.append([self.centroidX[self.frontier_index], self.centroidY[self.frontier_index]])
                        self.WPT_ID +=1
                        # Try next best frontier
                        self.centroidX.pop(self.frontier_index)
                        self.centroidY.pop(self.frontier_index)
                        self.cost.pop(self.frontier_index)
                        marker_index = self.centroidIndex[self.frontier_index]
                        self.centroidIndex.pop(self.frontier_index)
                        print '\tOld: ', marker_index, self.frontier_index, len(self.centroidIndex)
                        
                        # Mark the frontier that cannot be reached as red
                        self.markerArray.markers[marker_index].color.r = 1.0
                        self.markerArray.markers[marker_index].color.g = 0.0
                        self.markerArray.markers[marker_index].color.b = 0.0
                        
                        self.lineMarker.markers[marker_index].color.r = 1.0
                        self.lineMarker.markers[marker_index].color.g = 0.0
                        self.lineMarker.markers[marker_index].color.b = 0.0
                        
                        self.marker_pub.publish(self.markerArray)
                        self.lineMarker_pub.publish(self.lineMarker)
                        
                        self.frontier_index = self.updateBestCentoid()
                            
                        if self.frontier_index == -1 or len(self.centroidX)==0:
                            self.removeAllMarkers()
                            print '\tNo more frontiers'
                            return
                        self.move2frontier(self.centroidX[self.frontier_index], self.centroidY[self.frontier_index])
                            
                    elif self.move_base_status[self.WPT_ID] == GoalStatus.SUCCEEDED:
                        self.reached_centroids.append([self.centroidX[self.frontier_index], self.centroidY[self.frontier_index]])
                        self.start_spin = True
                        print 'Spining'   
                        self.spin360()
                        
            self.r.sleep()
                    


        self.removeAllMarkers()
        for i in range(5):
            self.r.sleep()

        '''
        print(centroidX, centroidY)
        self.move2frontier(centroidX, centroidY)

        
        while not rospy.is_shutdown() and len(self.mapp) > 0: # removed timing info
            try:
                # self.get_current_position()
                # If the robot is currently spinning, check if it completed a 
                #   rotation and if it has, find the frontiers.
                if self.robotState == RobotState.spin:
                    self.spin360()
                    mm = self.getFrontier()

                    centroidX, centroidY = self.pickBestCentroid(mm)

                    self.robotState = RobotState.move_base
                
                
                if self.robotState == RobotState.move_base:
                    self.move2frontier(centroidX, centroidY)
                    self.robotState = RobotState.spin


                print(self.robotState)
            except KeyboardInterrupt:
                print 'Exiting' 
            
            
        # At the end remove all rviz markers
        self.removeAllMarkers()
        for i in range(5):
            self.r.sleep()

        '''
        print 'exiting'
        
    def __init__(self):
        """ Initialize """
        rospy.init_node('Frontier_exploration')
        
        self.listener = tf.TransformListener()        
        
        self.mapp = {}

        # Get the parameters for the grid
        self.ogrid_sizeX = rospy.get_param('x_size', 500)
        self.ogrid_sizeY = rospy.get_param('y_size', 500)
        self.grid_size = rospy.get_param('grid_size', 0.05) # in meters/cell (5cm)
        
        # Sensor Meta data
        self.min_range = rospy.get_param('max_range',0.4)
        self.max_range = rospy.get_param('max_range',6.0)
        
        # reliability
        self.p_measurement_given_occupied = rospy.get_param('p_z|occ',0.9)
        self.p_measurement_given_notOccupied = rospy.get_param('p_z|notOcc',0.3)
        
        # Initialize some varables to store the objects to be published/subscribed
        self.position = Position()
        self.robotState = RobotState()
        self.frontierCentroid = Position()
        self.markerArray = MarkerArray()
        self.lineMarker = MarkerArray()
        self.cur_odom = Position()
        self.prev_odom = Position()
        
        self.current_map = OccupancyGrid()
        self.meta = MapMetaData()
        self.new_pose = PoseStamped()
        
        # Rotation Booleans
        self.start_spin = True
        self.done_spinning = True
        self.new_cmd = False
        
        self.unreachable_frontiers = []
        self.reached_centroids=[]
        self.WPT_ID = 0
        self.angle_trav = 0
        self.move_base_status = [0]
        
        self.origin = Position()
        
        self.r = rospy.Rate(50)
        
        # publishers for OccupancyGrid and move base messages
        self.marker_pub = rospy.Publisher('/centroid_marker', MarkerArray, queue_size=100)
        self.lineMarker_pub = rospy.Publisher('/frontier_marker', MarkerArray, queue_size=100)
        self.cmd_pose = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=100)
        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=100)
        
        # subscribers for odometry and position (/move_base/feedback) messages
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.odom_callback)
        self.sub_map = rospy.Subscriber('/frontier_map', OccupancyGrid, self.costmap_callback)
        self.sub_status = rospy.Subscriber('/move_base/status', GoalStatusArray, self.status_callback)
        self.base_pos_sub = rospy.Subscriber("/pose2D", Pose2D, self.pose_callback)
                
        self.run()
        
        return

if  __name__=="__main__":
    l = Frontier_Based_Exploration()

